# Web Page Basics
In this repo we have the example HTML/CSS code with all the options that we'd like to have taught during the workshop.

It also contains the Reveal.JS slides for the presentation itself.

# Contributors
@achalmers
@uwalter-laufs
@fvangemeren
