# Web page basics workshop @trivago
Duration: 2-4 hours
Number of participants: 
Frequency: once or twice a month

## What is the goal of this workshop?
1. Ask the participants what they expect of the workshop:
	1. Are you interested in coding yourself one day or after this workshop?
	2. Do you just want to grasp and get an idea how things work?
	3. 
2. Basic understanding of how HTML & CSS work?

## The workshop’s content
* Introduction to the structure and the topics of this workshop
* How does the web work
* Short history of the www
* What is HTML?
* What is CSS?
- (What is JavaScript)

## How does the web work?
- The world wide web is only a part of the internet, which is a distributed network of connected computers
- Distributed means, that there is no core to the network. 
- This makes it safer against complete outage or blockage through censorship e.g.
> a communication protocol is a system of rules that allow two or more entities of a communications system to transmit information – [source: Wikipedia](https://en.wikipedia.org/wiki/Communications_protocol)  
- The world wide web is communicating over the http protocol
- The connected computers in the web are either a server or a client.
	- Clients are the typical Web user’s Internet-connected devices (for example, your computer connected to your Wi-Fi, or your phone connected to your mobile network) and Web-accessing software available on those devices (usually a web browser like Firefox or Chrome).
	- Servers are computers that store webpages, sites, or apps. 
* When a client device wants to access a webpage, a copy of the webpage is downloaded from the server onto the client machine to be displayed in the user’s web browser.

![](Web%20page%20basics%20workshop%20@trivago/5191F23C-ED7E-4D66-B726-C0A7B2731E24.png)

## Short history of the World Wide Web
- It started at CERN („European Organisation for Nuclear Research“)
- Scientists at CERN have to deal with huge amounts of data
- Managing & sharing that information was a huge challenge
- Tim Berners-Lee started a personal project to handle all the information
- The internet (network of connected computers throughout the world) already existed since 1960ies
- Connected universities and scientific institutions
* The internet is a decentralised network. There’s no center to it.
- The protocols underlying the transmission of data on the internet — TCP/IP — describe how packets of data should be moved around, but those protocols care not for the contents of the packets
- That allows the internet to be the transport mechanism for all sorts of applications: email, Telnet, FTP, and eventually the **World Wide Web**
- The web uses **http** (HyperText Transfer Protocol), to send and receive data
- This data is uniquely identified with an **url** (Uniform Resource Locator)
- Many of these urls identify pages made of **HTML** (HyperText Markup Language)
-> *(That would be a good breakpoint to introduce HTML)*
- The killer feature of the web lies here in HTML’s humble `<a>` element. The A stands for Anchor. Its `href` attribute allows you to cast off from within one URL out to another URL, creating a link that can be traversed from one page to the other.
- These links turn the web into a hypertext system 
- Hypertext in comparison to the usual linear texts is characterised by being able to link to other relevant texts instead of forcing you into a linear reading direction.
- Tim Berners-Lee assumed that most urls would point to non-html resources; word-processing documents, spreadsheets, and all sorts of other computer files.
- people began writing content directly in **HTML**.

## What is HTML?
- Abbreviation for Hypertext Markup Language
- HTML is not a programming language; 
* it is a markup language, 
* it is used to display structured content of a webpage that the browser displays.
- HTML consists of a series of elements, which you use to enclose, or wrap, different parts of the content to make it appear a certain way, or act a certain way.

### Anatomy of an HTML element
![](Web%20page%20basics%20workshop%20@trivago/7B605F34-F80B-4C38-AD98-39168098D43A.png) 

1. **The opening tag:** This consists of the name of the element (in this case, p), wrapped in opening and closing angle brackets. This states where the element begins, or starts to take effect — in this case where the start of the paragraph is. 
2. **The closing tag:** This is the same as the opening tag, except that it includes a forward slash before the element name. This states where the element ends — in this case where the end of the paragraph is. Failing to include a closing tag is one of the common beginner errors and can lead to strange results.
3. **The content:** This is the content of the element, which in this case is just text.
4. **The element:** The opening tag, plus the closing tag, plus the content, equals the element.

### Attributes
Elements can also have attributes, which look like this:
![](Web%20page%20basics%20workshop%20@trivago/952D66A9-9500-4BFA-9C70-B97DCE2C2242.png)

An attribute should always have:
1. A space between it and the element name (or the previous attribute, if the element already has one or more attributes).
2. The attribute name

### Nesting elements
- You can put elements inside other elements too — this is called nesting.
- If we wanted to state that our cat is VERY grumpy, we could wrap the word "very" in a <strong> element, which means that the word is to be strongly emphasized:

```html
<p>My cat is <strong>very</strong> grumpy.</p>
```

- You do however need to make sure that your elements are properly nested: in the example above we opened the <p> element first, then the <strong> element, therefore we have to close the <strong> element first, then the <p>. The following is incorrect:

```html
<p>My cat is <strong>very grumpy.</p></strong>
```

- The elements have to open and close correctly so they are clearly inside or outside one another. If they overlap like above, then your web browser will try to make a best guess at what you were trying to say, and you may well get unexpected results. So don't do it!

### Empty elements
- Some elements have no content, and are called empty elements. For example the `<img>` element.

```html
<img src="images/firefox-icon.png" alt="My test image">
```

- This contains two attributes, but there is no closing </img> tag, and no inner content. This is because an image element doesn't wrap content to have an effect on it. Its purpose is to embed an image in the HTML page, in the place it appears.

### Anatomy of an HTML document
```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>My test page</title>
  </head>
  <body>
    <img src="images/firefox-icon.png" alt="My test image">
  </body>
</html>
```

* `<!DOCTYPE html>` — the doctype. In the mists of time, when HTML was young (about 1991/2), doctypes were meant to act as links to a set of rules that the HTML page had to follow to be considered good HTML, which could mean automatic error checking and other useful things. However, these days no-one really cares about them, and they are really just a historical artifact that needs to be included for everything to work right. For now, that's all you need to know.
* `<html></html>` — the `<html>` element. This element wraps all the content on the entire page, and is sometimes known as the root element.
* `<head></head>` — the `<head>` element. This element acts as a container for all the stuff you want to include on the HTML page that isn't the content you are showing to your page's viewers. This includes things like keywords and a page description that you want to appear in search results, CSS to style our content, character set declarations, and more.
* `<body></body>` — the `<body>` element. This contains all the content that you want to show to web users when they visit your page, whether that's text, images, videos, games, playable audio tracks, or whatever else.
* `<meta charset="utf-8">` — this element sets the character set your document should use to utf-8, which includes most characters from all known human languages. Essentially it can now handle any textual content you might put on it. There is no reason not to set this, and it can help avoid some problems later on.
* `<title></title>` — this sets the title of your page, which is the title that appears in the browser tab the page is loaded in, and is used to describe the page when you bookmark/favourite it.

## What is CSS?
* Abbreviation for Cascading Stylesheets
* No programming language - declarative language to declare styles to a marked up document
* HTML is used to define the structure and semantics of your content, CSS is used to style it and lay it out.
* for example, you can use CSS to alter the font, color, size and spacing of your content, split it into multiple columns, or add animations and other decorative features.

### How does CSS affect HTML?
* Web browsers apply CSS rules to a document to affect how they are displayed. A CSS rule is formed from:
	* A **set of properties**, which have values set to update how the HTML content is displayed, for example I want my element's width to be 50% of its parent element, and its background to be red.
	* A **selector**, which selects the element(s) you want to apply the updated property values to. For example, I want to apply my CSS rule to all the paragraphs in my HTML document.

### CSS Syntax
At its most basic level, CSS consists of two building blocks:

#### Properties 
* Human-readable identifiers that indicate which stylistic features (e.g. `font`, `width`, `background color`) you want to change.
* There are more than **300** different properties in CSS

#### Values 
Each specified property is given a value, which indicates how you want to change those stylistic features (e.g. what you want to change the font, width or background color to.)

#### CSS declaration
A property paired with a value is called a **CSS Declaration**
```css
	background-color: green
```
the colon (:) separates the two entities

#### CSS declaration block
CSS declarations are put within **CSS Declaration Blocks**.
```css
{
	background-color: green; 
	font-weight: bold
}
```
* the semicolon (;) separates the two declarations
* the final semicolon is not necessary, but good practice

#### CSS selectors and rules
* CSS declarations are prefixed with a **selector**
* It’s a pattern that matches some elements on the page (html document or DOM)
* The associated declarations will be applied to those elements only
```css
h1, .green-heading, #specific-heading {
	background-color: green; 
	font-weight: bold
}
```
* The selector plus the declaration block is called ruleset or just a CSS rule
* Selectors can get very complicated — you can make a rule match multiple elements by including multiple selectors separated by commas (a group,) and selectors can be chained together, for example: 
- - - -
> *I want to select any element with a class of "blah", but only if it is inside an* `<article>` *element, and only while it is being hovered by the mouse pointer.*  
- - - -
#### Shorthand
* Some properties like `font`, `background`, `padding`, `border`, and `margin` are called shorthand properties 
* This is because they allow you to set several property values in a single line, saving time and making your code neater in the process.

```css
/* in shorthand like padding and margin, the values are applied
   in the order top, right, bottom, left (the same order as an analog clock). There are also other 
   shorthand types, for example two values, which set for example
   the padding for top/bottom, then left/right */
padding: 10px 15px 15px 5px;
```

```css
padding-top: 10px;
padding-right: 15px;
padding-bottom: 15px;
padding-left: 5px;
```

### Selectors
There are different types of selectors:

#### Simple selectors (**This is important**)
##### Type/element selector
* This selector is just a case-insensitive match between the selector name and a given HTML element name.
* This is the simplest way to target all elements of a given type. 

```html
<p>What color do you like?</p>
<div>I like blue.</div>
<p>I prefer red!</p>
```

```css
/* All p elements' foreground colors are red */
p {
  color: red;
}

/* All div elements' foreground colors are blue */
div {
  color: blue;
}
```

##### Class selector
* The class selector consists of a dot, '.', followed by a class name
* A class name is any value without spaces put within an **HTML** class attribute.
* multiple elements in a document can have the same class value
* a single element can have multiple class names separated by white space

```html
<ul>
  <li class="first done">Create an HTML document</li>
  <li class="second done">Create a CSS style sheet</li>
  <li class="third">Link them all together</li>
</ul>
```

```css
/* The element with the class "first" is bolded */
.first {
  font-weight: bold;
}

/* All the elements with the class "done" are strike through */
.done {
  text-decoration: line-through;
}
```

##### ID selector
* The ID selector consists of a hash/pound symbol (\#), followed by the ID name of a given element
* Any element can have a unique ID name set with the id attribute

> **Important:** An ID name must be unique in the document. Behaviours regarding duplicated IDs are unpredictable, for example in some browsers only the first instance is counted, and the rest are ignored.  

```html
<p id="polite"> — "Good morning."</p>
<p id="rude"> — "Go away!"</p>
```

```css
#polite {
  font-family: cursive;
}

#rude {
  font-family: monospace;
  text-transform: uppercase;
}
```

##### Universal selector
* The universal selector (*) is the ultimate joker
* It allows selecting all elements in a page
* As it is rarely useful to apply a style to every element on a page, it is often used in combination with other selectors

> **Important:** Careful when using the universal selector. As it applies to all elements, using it in large web pages can have a perceptible impact on performance: web pages can be displayed slower than expected. There are not many instances where you'd want to use this selector.  

```html
<div>
  <p>I think the containing box just needed
  a <strong>border</strong> or <em>something</em>,
  but this is getting <strong>out of hand</strong>!</p>
</div>
```

```css
* {
  padding: 5px;
  border: 1px solid black;
  background: rgba(255,0,0,0.25)
}
```

#### Attribute selectors
* Attribute selectors are a special kind of selector that will match elements based on their attributes and attribute values
* Their generic syntax consists of square brackets ([]) containing an attribute name followed by an optional condition to match against the value of the attribute

##### Presence and value attribute selectors
These attribute selectors try to match an exact attribute value:
* `[attr]` : This selector will select all elements with the attribute `attr`, whatever its value.
* `[attr=val]` : This selector will select all elements with the attribute `attr`, but only if its value is val.
* `[attr~=val]`: This selector will select all elements with the attribute `attr`, but only if the value val is one of a space-separated list of values contained in `attr`'s value, for example a single class in a space-separated list of classes.

```html
<ul>
  <li data-quantity="1kg" data-vegetable>Tomatoes</li>
  <li data-quantity="3" data-vegetable>Onions</li>
  <li data-quantity="3" data-vegetable>Garlic</li>
  <li data-quantity="700g" data-vegetable="not spicy like chili">Red pepper</li>
  <li data-quantity="2kg" data-meat>Chicken</li>
  <li data-quantity="optional 150g" data-meat>Bacon bits</li>
  <li data-quantity="optional 10ml" data-vegetable="liquid">Olive oil</li>
  <li data-quantity="25cl" data-vegetable="liquid">White wine</li>
</ul>
```

```css
/* All elements with the attribute "data-vegetable"
   are given green text */
[data-vegetable] {
  color: green;
}

/* All elements with the attribute "data-vegetable"
   with the exact value "liquid" are given a golden
   background color */
[data-vegetable="liquid"] {
  background-color: goldenrod;
}

/* All elements with the attribute "data-vegetable",
   containing the value "spicy", even among others,
   are given a red text color */
[data-vegetable~="spicy"] {
  color: red;
}
```

> Note: The data-* attributes seen in this example are called data attributes. They provide a way to store custom data in an HTML attribute so it can then be easily extracted and used. See How to use data attributes for more information.  

##### Substring value attribute selectors
They offer flexible matching:
* `[attr|=val] `: This selector will select all elements with the attribute `attr` for which the value is exactly `val` or starts with `val-` (careful, the dash here isn't a mistake, this is to handle language codes.)
* `[attr^=val] `: This selector will select all elements with the attribute `attr` for which the value starts with `val`.
* `[attr$=val] `: This selector will select all elements with the attribute `attr` for which the value ends with `val`.
* `[attr*=val]` : This selector will select all elements with the attribute `attr` for which the value contains the string `val` (unlike `[attr~=val]`, this selector doesn't treat spaces as value separators but as part of the attribute value.)

```css
/* Classic usage for language selection */
[lang|=fr] {
  font-weight: bold;
}

/* All elements with the attribute "data-quantity", for which
   the value starts with "optional" */
[data-quantity^="optional"] {
  opacity: 0.5;
}

/* All elements with the attribute "data-quantity", for which
   the value ends with "kg" */
[data-quantity$="kg"] {
  font-weight: bold;
}

/* All elements with the attribute "data-vegetable" containing
   the value "not spicy" are turned back to green */
[data-vegetable*="not spicy"] {
  color: green;
}
```

#### Pseudo-classes and pseudo-elements
These don't select actual elements, but rather certain parts of elements, or elements only in certain contexts

##### Pseudo-classes
A CSS pseudo-class is a keyword preceded by a colon (:) that is added on to the end of selectors to specify that you want to style the selected elements only when they are in certain state

```html
<a href="https://developer.mozilla.org/" target="_blank">Mozilla Developer Network</a>
```

```css
/* These styles will style our link
   in all states */
a {
  color: blue;
  font-weight: bold;
}

/* We want visited links to be the same color
   as non visited links */
a:visited {
  color: blue;
}

/* We highlight the link when it is
   hovered (mouse), activated
   or focused (keyboard) */
a:hover,
a:active,
a:focus {
  color: darkred;
  text-decoration: none;
}
```

##### Pseudo-elements
They are keywords — this time preceded by two colons (`::`) — that can be added to the end of selectors to select a certain part of an element.
* `::after`
* `::before`
* `::first-letter`
* `::first-line`
* `::selection`
* `::backdrop`

```html
<ul>
  <li><a href="https://developer.mozilla.org/en-US/docs/Glossary/CSS">CSS</a> defined in the MDN glossary.</li>
  <li><a href="https://developer.mozilla.org/en-US/docs/Glossary/HTML">HTML</a> defined in the MDN glossary.</li>
</ul>

```

```css
/* All elements with an attribute "href", which values
   start with "http", will be added an arrow after its
   content (to indicate it's an external link) */
[href^=http]::after {
  content: '⤴';
}
```

#### Combinators and Multiple selectors
##### Combinators
* Using one selector at a time is useful, but can be inefficient in some situations
* More useful when you start combining them to perform fine-grained selections
* Select elements based on how they are related to one another
* Those relationships are expressed with combinators as follows (A and B represent any selector seen above)
![](Web%20page%20basics%20workshop%20@trivago/Combinators_and_multiple_selectors_-_Learn_web_development___MDN.jpg)

### CSS values and units

#### Numeric values
##### Length and size
##### Unitless values

#### Percentages

#### Colors
##### Keywords
##### Hexadecimal values
##### RGB
##### HSL
##### RGBA and HSLA
##### Opacity

#### Functions

### Cascade and inheritance

#### The cascade
##### Importance
##### Specificity
##### Source order

#### Inheritance
##### Controlling inheritance

### The box model
#### Box properties
#### Advanced box manipulation
##### Overflow
##### Background clip
##### Outline
#### Types of CSS boxes

### CSS statements (optional)
- CSS rules
- At-rules
- Nested statements

- - - -
> **Explain how CSS ignores properties, values, units and selectors that are not valid!**  

- [ ] Prepare coding parts for HTML -> Frank
- [ ] Prepare coding parts for CSS -> Ulf
- [ ] Prepare „What is CSS“ section -> Ulf
- [ ] Maybe prepare something for JavaScript (explanation) 
- [ ] Prepare slides for the content -> Frank
- [ ] Cheatsheet for HTML -> Andrea
- [ ] Cheatsheet for CSS -> Andrea

- - - -
#### Sources:
1. Jeremy Keith. „Resilient Web Design.“
2. https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/
3. [Hypertext - Wikipedia](https://en.wikipedia.org/wiki/Hypertext)